let express = require('express');
let router = express.Router();
let response = require('./Response');
let UserDetail = require('../schema/UserDetailSchema');
let Productdetail = require('../schema/ProductDetailSchema');
let QuoteDetail = require('../schema/QuoteDetailSchema');
let VendorDetail = require('../schema/VendorDetailSchema');
let Categorydetail = require('../schema/CategoryDetailSchema');
let CustomerVisits = require('../schema/CustomersVisitsSchema');
let UserType = require('../schema/UserTypeSchema');
let verifyToken = require('../controllers');

let jwt = require('jsonwebtoken');

router.post("/register", async function (req, res) {
    console.log("Enter: register");
    try {
        let userDetail = new UserDetail(req.body);
        userDetail.user_id = Date.now();
        let result = await userDetail.save();
        console.log("Success");
        response.success(req, res, "Success register", userDetail);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.post("/login", async function (req, res) {
    console.log("Enter: login");
    try {
        let result = await UserDetail.findOne({ 'email': req.body.email, 'password': req.body.password });
        console.log("Success");
        console.log(result);
        let token = jwt.sign({ username: result.username, email: result.email}, "superSec");
        response.success(req, res, "Success login", token);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.post("/addUser", async function (req, res) {
    console.log("Enter: addUser");
    try {
        let userDetail = new UserDetail(req.body);
        userDetail.user_id = Date.now();
        let result = await userDetail.save();
        console.log("Success");
        response.success(req, res, "Success addUser", userDetail);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/addVendor", async function (req, res) {
    console.log("Enter: addVendor");
    try {
        let vendorDetail = new VendorDetail(req.body);
        vendorDetail.vendor_id = Date.now();
        let result = await vendorDetail.save();
        console.log("Success");
        response.success(req, res, "Success addVendor", vendorDetail);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/updateUser", async function (req, res) {
    try {
        let user_id = req.body.user_id;
        let result = await UserDetail.updateOne({ 'user_id': user_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success updateUser", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/updateVendor", async function (req, res) {
    try {
        let vendor_id = req.body.vendor_id;
        let result = await VendorDetail.updateOne({ 'vendor_id': vendor_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success updateVendor", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/updateProduct", async function (req, res) {
    try {
        let product_id = req.body.product_id;
        let result = await Productdetail.updateOne({ 'product_id': product_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success updateProduct", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/updateQuote", async function (req, res) {
    try {
        let quote_id = req.body.quote_id;
        let result = await QuoteDetail.updateOne({ 'quote_id': quote_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success updateQuote", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});


router.post("/addProduct", async function (req, res) {
    console.log("Enter: addProduct");
    try {
        let productdetail = new Productdetail(req.body);
        productdetail.product_id = Date.now();  
        let result = await productdetail.save();
        console.log("Success");
        response.success(req, res, "Success addProduct", productdetail);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/addQuote", async function (req, res) {
    console.log("Enter: addQuote");
    try {
        let quoteDetail = new QuoteDetail(req.body);
        quoteDetail.quote_id = Date.now();
        let result = await quoteDetail.save();
        console.log("Success");
        response.success(req, res, "Success addQuote", quoteDetail);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});


router.get("/getAllUsers", async function (req, res) {
    console.log("Enter: getAllUsers");
    try {
        let result = await UserDetail.find();
        console.log("Success");
        response.success(req, res, "Success getAllUsers", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.post("/getUsersByUserType", async function (req, res) {
    console.log("Enter: getUsersByUserType");
    try {
        let result = await UserDetail.find({usertypename: req.body.userType});
        console.log("Success");
        response.success(req, res, "Success getUsersByUserType", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getUsers", async function (req, res) {
    console.log("Enter: getAllUsers");
    try {
        let result = await UserDetail.find();
        console.log("Success");
        response.success(req, res, "Success getAllUsers", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});


router.get("/getAllProducts", async function (req, res) {
    console.log("Enter: getAllProducts");
    try {
        let result = await Productdetail.find();
        console.log("Success");
        response.success(req, res, "Success getAllProducts", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});


router.get("/getAllQuotes", async function (req, res) {
    console.log("Enter: getAllQuotes");
    try {
        let result = await QuoteDetail.find();
        console.log("Success");
        response.success(req, res, "Success getAllQuotes", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getAllVendors", async function (req, res) {
    console.log("Enter: getAllVendors");
    try {
        let result = await VendorDetail.find();
        console.log("Success");
        response.success(req, res, "Success getAllVendors", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});




router.get("/getUser/:id", async function (req, res) {
    console.log("Enter: getUser");
    try {
        console.log(req.params);
        let result = await UserDetail.findOne({ 'user_id': req.params.id }, req.body);
        console.log("Success", result);
        response.success(req, res, "Success getUser", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getVendors/:id", async function (req, res) {
    console.log("Enter: getVendor");
    try {
        console.log(req.params);
        let result = await VendorDetail.findOne({ 'vendor_id': req.params.id }, req.body);
        console.log("Success", result);
        response.success(req, res, "Success getVendor", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getQuote/:id", async function (req, res) {
    console.log("Enter: getQuote");
    try {
        console.log(req.params);
        let result = await QuoteDetail.findOne({ 'quote_id': req.params.id }, req.body);
        console.log("Success", result);
        response.success(req, res, "Success getQuote", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getProduct/:id", async function (req, res) {
    console.log("Enter: getProduct");
    try {
        console.log(req.params);
        let result = await Productdetail.findOne({ 'product_id': req.params.id }, req.body);
        console.log("Success", result);
        response.success(req, res, "Success getProduct", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});


router.post("/deleteUser", async function (req, res) {
    try {
        let user_id = req.body.user_id;
        let result = await UserDetail.deleteOne({ 'user_id': user_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success deleteUser", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/deleteVendor", async function (req, res) {
    try {
        let vendor_id = req.body.vendor_id;
        let result = await VendorDetail.deleteOne({ 'vendor_id': vendor_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success deleteVendor", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/deleteProduct", async function (req, res) {
    try {
        let product_id = req.body.product_id;
        let result = await Productdetail.deleteOne({ 'product_id': product_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success deleteProduct", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/deleteQuote", async function (req, res) {
    try {
        let quote_id = req.body.quote_id;
        let result = await QuoteDetail.deleteOne({ 'quote_id': quote_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success deleteQuote", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.post("/updateCategory", async function (req, res) {
    try {
        let product_id = req.body.category_id;
        let result = await Categorydetail.updateOne({ 'category_id': category_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success updateCategory", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/addCategory", async function (req, res) {
    console.log("Enter: addCategory");
    try {
        let categorydetail = new Categorydetail(req.body);
        categorydetail.category_id = Date.now();  
        let result = await categorydetail.save();
        console.log("Success");
        response.success(req, res, "Success addCategory", categorydetail);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getAllCategory", async function (req, res) {
    console.log("Enter: getAllCategory");
    try {
        let result = await Categorydetail.find();
        console.log("Success");
        response.success(req, res, "Success getAllCategory", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.get("/getCategory/:id", async function (req, res) {
    console.log("Enter: getCategory");
    try {
        console.log(req.params);
        let result = await Categorydetail.findOne({ 'category_id': req.params.id }, req.body);
        console.log("Success", result);
        response.success(req, res, "Success getCategory", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.post("/deleteCategory", async function (req, res) {
    try {
        let category_id = req.body.category_id;
        let result = await Categorydetail.deleteOne({ 'category_id': category_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success deleteCategory", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getTotalUsers", async function (req, res){
    console.log("Enter: getTotalUsers");
    try {
        console.log(req.params);
        let result = await UserDetail.find().count();
        console.log("Success", result);
        response.success(req, res, "Success getTotalUsers", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getTotalQuotes", async function (req, res){
    console.log("Enter: getTotalQuotes");
    try {
        console.log(req.params);
        let result = await QuoteDetail.find().count();
        console.log("Success", result);
        response.success(req, res, "Success getTotalQuotes", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getTotalProducts", async function (req, res){
    console.log("Enter: getTotalProducts");
    try {
        console.log(req.params);
        let result = await Productdetail.find().count();
        console.log("Success", result);
        response.success(req, res, "Success getTotalProducts", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getTotalVendors", async function (req, res){
    console.log("Enter: getTotalVendors");
    try {
        console.log(req.params);
        let result = await VendorDetail.find().count();
        console.log("Success", result);
        response.success(req, res, "Success getTotalProducts", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.get("/getTotalVisits", async function (req, res){
    console.log("Enter: getTotalVisits");
    try {
        console.log(req.params);
        let result = await CustomerVisits.find().count();
        console.log("Success", result);
        response.success(req, res, "Success getTotalVisits", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});


router.post("/updateUserType", async function (req, res) {
    try {
        let usertype_id = req.body.usertype_id;
        let result = await UserType.updateOne({ 'usertype_id': usertype_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success updateUserType", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.post("/addUserType", async function (req, res) {
    console.log("Enter: addCategory");
    try {
        let usertype = new UserType(req.body);
        usertype.usertype_id = Date.now();  
        let result = await usertype.save();
        console.log("Success");
        response.success(req, res, "Success addCategory", usertype);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

router.get("/getAllUserType", async function (req, res) {
    console.log("Enter: getAllUserType");
    try {
        let result = await UserType.find();
        console.log("Success");
        response.success(req, res, "Success getAllUserType", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});


router.get("/getUserType/:id", async function (req, res) {
    console.log("Enter: getUserType");
    try {
        console.log(req.params);
        let result = await UserType.findOne({ 'usertype_id': req.params.id }, req.body);
        console.log("Success", result);
        response.success(req, res, "Success getUserType", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});



router.post("/deleteUserType", async function (req, res) {
    try {
        let usertype_id = req.body.usertype_id;
        let result = await UserType.deleteOne({ 'usertype_id': usertype_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success deleteUserType", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});

///////////////////////// -- Visits code -- /////////////////////////
router.post("/addVisit", async function (req, res) {
    console.log("Enter: addVisits");
    try {
        let visitDetail = new CustomerVisits(req.body);
        //visitDetail.visit_id = Date.now();
        let result = await visitDetail.save();
        console.log("Success");
        response.success(req, res, "Success addVisits", visitDetail);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.get("/getVisit/:id", async function (req, res) {
    console.log("Enter: getVisit");
    try {
        console.log(req.params);
        let result = await CustomerVisits.findOne({ 'visit_id': req.params.id }, req.body);
        console.log("Success", result);
        response.success(req, res, "Success getVisit", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.get("/getAllVisits", async function (req, res) {
    console.log("Enter: getAllVisits");
    try {
        let result = await CustomerVisits.find();
        console.log("Success");
        response.success(req, res, "Success getAllVisits", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.post("/getVisitsBetweenDates", async function (req, res) {
    console.log("Enter: getVisitsBetweenDates");
    try {
        let result = await CustomerVisits.find({visit_id: { $gte: req.body.from, $lt: req.body.to}});
        console.log("Success");
        response.success(req, res, "Success getAllVisits", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.post("/updateVisit", async function (req, res) {
    try {
        let visit_id = req.body.visit_id;
        let result = await CustomerVisits.updateOne({ 'visit_id': visit_id }, req.body);
        console.log("Success");
        response.success(req, res, "Success updateVisit", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
router.post("/deleteVisit", async function (req, res) {
    try {
        let visit_id = req.body.visit_id;
        let result = await CustomerVisits.deleteOne({ 'visit_id': visit_id }, req.body);
        console.log("Success");
        response.success(req, res, "Customer Visit deleted", result);
    } catch (error) {
        console.log("Failure:", error);
        response.serverError(req, res, error.message, error);
    }
});
//////////
module.exports = router;
