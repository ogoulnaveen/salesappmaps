import { Component, OnInit } from '@angular/core';

import { DBService } from 'src/app/services/dbservice.service';
import { CategoryDetail } from '../../../models/CategoryDetail.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomLogger } from '../../../models/utils/CustomLogger';
import { CustomMisc } from '../../../models/utils/CustomMisc';

@Component({
  selector: 'app-categorylist',
  templateUrl: './categorylist.component.html',
  styleUrls: ['./categorylist.component.css']
})
export class CategorylistComponent implements OnInit {

 

  constructor(private _dbService: DBService, private _router: Router, private _activatedRoute: ActivatedRoute) { }
  isUpdate = false;
  categoryDetail : CategoryDetail;
  filteredTableDataArr: any;
  categoryDataArr = [];
  
 
  async init() {
    let result = await this._dbService.getAllCategory().toPromise();
    console.log("result:", result);
    this.categoryDataArr = result["data"];
    this.filteredTableDataArr = this.categoryDataArr;
  
  }
 async ngOnInit() {
  await this.init();}

  
  
onClickEdit(obj) {
  this._router.navigate(['/products/categoryadd ', obj.categoryname]);
}
async onClickDelete(obj) {
  console.log("will delete Category:::", obj);
 
  await this._dbService.deleteCategory(obj).toPromise();
  await this.init();} 
} 