import { Component, OnInit } from '@angular/core';
import { DBService } from 'src/app/services/dbservice.service';
import {QuoteDetail } from 'src/app/models/QuoteDetail.model';
import { CategoryDetail } from 'src/app/models/CategoryDetail.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomLogger } from '../../../models/utils/CustomLogger';
import { CustomMisc } from '../../../models/utils/CustomMisc';

@Component({
  selector: 'app-createquotation',
  templateUrl: './createquotation.component.html',
  styleUrls: ['./createquotation.component.css']
})
export class CreatequotationComponent implements OnInit {

  quoteDetail : QuoteDetail;
  categoryDetail: CategoryDetail[] = [];
  constructor(private _dbService: DBService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  isUpdate = false;
  async htmlInit() {
    
    let result = await this._dbService.getAllCategory().toPromise();
    this.categoryDetail = result["data"];
} 
catch (error) {
    CustomLogger.logStringWithObject("ERROR:", error);
}

  ngOnInit() {
    this.quoteDetail = new QuoteDetail();
    this.htmlInit();
    this._activatedRoute.params.subscribe(
      async params => {
        console.log("params:", params);
        let quote_id = params["id"];
        if (quote_id) {
          let result = await this._dbService.getQuote(quote_id).toPromise();
          console.log(result);
          this.quoteDetail = result["data"];
          this.isUpdate = true;
        }

      }
    ); 

  }

  async onSubmit() {
    CustomLogger.logStringWithObject("Will save user...", this.quoteDetail);
try {
      let result = null;
      if (this.isUpdate)
      result = await this._dbService.updateQuote(this.quoteDetail).toPromise();
  else
  result = await this._dbService.addQuote(this.quoteDetail).toPromise();

  CustomLogger.logStringWithObject("addQuotation:result:", result);
  if (!this.isUpdate)
      CustomMisc.showAlert("Qotation Added Successfully");
  else
      CustomMisc.showAlert("Quotation Updated Successfully");
  this._router.navigate(["quotation/quotationlist"]);

} 
catch (error) {
  CustomLogger.logError(error);
  CustomMisc.showAlert("Error in adding Qotation: " + error.message, true);
}    

  }

}

 /*  ngOnInit() {
    console.log("11111111111");

    this.quoteDetail = new QuoteDetail();
  }

  async onClickSave(){
    console.log("Will save user...", this.quoteDetail);
    let result = await this._dbService.addQuote(this.quoteDetail).toPromise();
  
    console.log("Result:", result);

  }

}
 */



   