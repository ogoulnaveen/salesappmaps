import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { Utility } from '../services/utility.service';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
 
@Injectable()
export class AuthGuard implements CanActivate{
  constructor(private router: Router, private util: Utility) { }
 
  canActivate(): boolean {
    if (!this.util.isAuthenticated()) {
      this.router.navigate(['auth/login']);
      return false;
    }
    return true;
  }
}