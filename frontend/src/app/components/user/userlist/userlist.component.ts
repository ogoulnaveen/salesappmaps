import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DBService } from 'src/app/services/dbservice.service';

import * as xlsx from 'xlsx';
//import * as jsPDF from 'jspdf';\
declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');


@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  constructor(private _dbService: DBService, private _router: Router) { }

  tableDataArr = [];
  filteredTableDataArr: any;
  flagShowUserReport = true;



  async init() {
    let result = await this._dbService.getAllUsers().toPromise();
    console.log("result:", result);
    this.tableDataArr = result["data"];
    this.filteredTableDataArr = this.tableDataArr;
  }
  async ngOnInit() {
    await this.init();
  }

  onClickEdit(obj) {
    this._router.navigate(['/user/adduser', obj.user_id]);
  }

  async onClickDelete(obj) {
    console.log("will delete user:::", obj);
    await this._dbService.deleteUser(obj).toPromise();
    await this.init();
  }


  search(term: string) {
    let fieldName = "username";
    this.filteredTableDataArr = this.tableDataArr.filter(x =>
      x[fieldName].trim().toLowerCase().includes(term.trim().toLowerCase())
    );
  }

  
  fileName= 'userlist.excel'; 
exportToExcel() {
  let element = null;
  if (this.flagShowUserReport) { 
    this.fileName = "Userlist.xlsx";
    //let element = document.getElementById('content4'); 
    const ws: xlsx.WorkSheet = xlsx.utils.json_to_sheet(this.tableDataArr);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    xlsx.writeFile(wb, this.fileName);
  
  }
}

downloadPdf(){
  let data = null;
  const doc = new jsPDF();
  let fileName = "Userlist.pdf";
  if (this.flagShowUserReport) {
    fileName = "Userlist.pdf";
    //data = document.getElementById('content2');
    const col = ["UserId", "Username", "First Name", "Last Name"];
    const rows = [];
     
  for (let k = 0; k < this.tableDataArr.length; k++) {
    var temp = [this.tableDataArr[k].user_id, this.tableDataArr[k].username,
    this.tableDataArr[k].first_name, this.tableDataArr[k].last_name
    
    ];
    rows.push(temp);
    }
    const header = function (data) {
      doc.setFontSize(18);
      doc.setTextColor(30);
      doc.setFontStyle('normal');
//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
      doc.text("Report for User List", data.settings.margin.left, 50);
  };
    
  doc.autoTable(col, rows,{
    theme : 'grid',
   /*  theme: 'striped' */
    styles: {
            halign: 'right',},
    headerStyles: {
                 fillColor: [0, 65, 69],halign:'center'},
                 margin: {top: 60}, beforePageContent: header,
        
    columnStyles:{
         0: {halign:'left'}
    },
   

});

    doc.save(fileName);

  }

}
}
