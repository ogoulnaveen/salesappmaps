import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { SchedulevisitComponent } from './schedulevisit/schedulevisit.component';
import { ManagevisitsRoutingModule} from './managevisit.routing'
import { DataTableModule } from 'angular-6-datatable';
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  imports: [
  
    SharedModule,
    ManagevisitsRoutingModule,
    DataTableModule,
    HttpClientModule,
   
  ],
  declarations: [
    SchedulevisitComponent
    
  ],
})
export class ManagevisitModule { }
