import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './layouts/admin/admin.component';
import { AuthComponent } from './layouts/auth/auth.component';
import {AuthGuard } from './gaurds/auth.gaurd'

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      }, {
        path: 'basic',
        loadChildren: () => import('./components/basic/basic.module').then(m => m.BasicModule)
      }, {
        path: 'notifications',
        loadChildren: () => import('./components/advance/notifications/notifications.module').then(m => m.NotificationsModule)
      }, {
        path: 'forms',
        loadChildren: () => import('./components/forms/basic-elements/basic-elements.module').then(m => m.BasicElementsModule)
      }, {
        path: 'bootstrap-table',
        loadChildren: () => import('./components/tables/bootstrap-table/basic-bootstrap/basic-bootstrap.module').then(m => m.BasicBootstrapModule),
      }, {
        path: 'map',
        loadChildren: () => import('./map/google-map/google-map.module').then(m => m.GoogleMapModule),
      }, {
        path: 'simple-page',
        loadChildren: () => import('./simple-page/simple-page.module').then(m => m.SimplePageModule)
      },
      {
        path: 'createusers',
        loadChildren: () => import('./users/createusers/createusers.module').then(m => m.CreateusersModule)
      }
      ,
      {
        path: 'userslist',
        loadChildren: () => import('./users/userlist/userlist.module').then(m => m.userlistModule)
      }

      ,
      {
        path: 'createproducts',
        loadChildren: () => import('./products/createproducts/createproducts.module').then(m => m.CreateproductsModule)
      }
      ,
      {
        path: 'productslist',
        loadChildren: () => import('./products/productslist/productslist.module').then(m => m.ProductslistModule)
      }
      ,
      {
        path: 'createquotation',
        loadChildren: () => import('./quotation/createquotation/createquotation.module').then(m => m.CreatequotatioModule)
      }
      ,
      {
        path: 'quotationlist',
        loadChildren: () => import('./quotation/quotationlist/quotationlist.module').then(m => m.QuotationlistModule)
      },

      {
        path: 'user',
        loadChildren: () => import('./components/user/user.module').then(m => m.UserModule)
      },
      {
        path: 'products',
        loadChildren: () => import('./components/products/products.module').then(m => m.ProductsModule)
      },
      {
        path: 'quotation',
        loadChildren: () => import('./components/quotation/quotation.module').then(m => m.CreatequotationModule)
      },
      {
        path: 'vendors',
        loadChildren: () => import('./components/vendors/vendors.module').then(m => m.VendorModule)
      },
      {
        path: 'visitmode',
        loadChildren: () => import('./components/visitmode/visitmode.module').then(m => m.VisitmodeModule)
      },
      // {
      //   path: 'visitmode',
      //   loadChildren: () => import('./components/visitmode/visitmode.module').then(m => m.VisitmodeModule)
      // },
      {
        path: 'managevisit',
        loadChildren: () => import('./components/managevisit/managevisit.module').then(m => m.ManagevisitModule)
      },
      /*  {
         path: 'category',
         loadChildren: () => import('./components/category/category.module').then(m => m.CategoryModule)  }, */
    ]
  },
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
