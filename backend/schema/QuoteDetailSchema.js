const mongoose = require("mongoose");

const QuoteDetailSchema = mongoose.Schema({
    quote_id: { type: String, required: true },
    quotationnumber: { type: String},
    customername: { type: String},
    mobile: { type: String},
    quotationdate : { type: String},
    productname : { type: String},
    categoryname: {type: String},
    qnty : { type: String},
    rate : { type: String},
    discount  : { type: String},
    total  : { type: String}

});
module.exports = mongoose.model('QuoteDetails', QuoteDetailSchema, 'quote_details');
