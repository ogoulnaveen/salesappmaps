export class VisitDetails {
    visit_id: number;
    customer_name: string;
    email: string;
    phone: string;
    first_name: string;
    last_name: string;
    address: string;
    visit_added_by_username:string;
    visit_added_by_name: string;
    visit_added_by_email: string;
    visits_note: string;
    lat: number;
    long: number;
}