import { Component, OnInit } from '@angular/core';
import { CategoryDetail } from 'src/app/models/CategoryDetail.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomLogger } from '../../../models/utils/CustomLogger';
import { CustomMisc } from '../../../models/utils/CustomMisc';
import { DBService } from 'src/app/services/dbservice.service';
import * as xlsx from 'xlsx';
//import * as jsPDF from 'jspdf';\
declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {
 
  
  constructor(private _dbService: DBService, private _router: Router, private _activatedRoute: ActivatedRoute) { }
 
 

  
  productDataArr = [];
  filteredTableDataArr: any;
  flagShowProductReport = true;

  
  categoryDetail = [];

  async init() {
    let result = await this._dbService.getAllProducts().toPromise();
    console.log("result:", result);
    this.productDataArr = result["data"];
    this.filteredTableDataArr = this.productDataArr;
  
  }
 /*  async htmlInit() {
    let result = await this._dbService.getAllCategory().toPromise();
    console.log("result:", result);
    this.categoryDetail = result["data"];
    this.filteredTableDataArr = this.categoryDetail;
  
  } */
 


  async ngOnInit() {
    await this.init();
  /*   await this.htmlInit(); */
   

  }

  onClickEdit(obj) {
    this._router.navigate(['/products/createproduct', obj.product_id]);
  }

  async onClickDelete(obj) {
    console.log("will delete Product:::", obj);

    await this._dbService.deleteProduct(obj).toPromise();
    await this.init();
   
  }

  search(term: string) {
    let fieldName = "productname";
    this.filteredTableDataArr = this.productDataArr.filter(x =>
      x[fieldName].trim().toLowerCase().includes(term.trim().toLowerCase())
    );
  }

  fileName= 'Productlist.excel'; 
  exportToExcel() {
    let element = null;
    if (this.flagShowProductReport) { 
      this.fileName = "Productlist.xlsx";
      //let element = document.getElementById('content4'); 
      const ws: xlsx.WorkSheet = xlsx.utils.json_to_sheet(this.productDataArr);
      const wb: xlsx.WorkBook = xlsx.utils.book_new();
      xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
  
      /* save to file */
      xlsx.writeFile(wb, this.fileName);
    
    }
  }
  
downloadPdf(){
  let data = null;
  const doc = new jsPDF();
  let fileName = "Productlist.pdf";
  if (this.flagShowProductReport) {
    fileName = "Productlist.pdf";
    //data = document.getElementById('content2');
    const col = ["ProductId", "Product Name", "Cost price", "Supplier Name"];
    const rows = [];
     
  for (let k = 0; k < this.productDataArr.length; k++) {
    var temp = [this.productDataArr[k].product_id, this.productDataArr[k].productname,
    this.productDataArr[k].cost, this.productDataArr[k].suppliername
    
    ];
    rows.push(temp);
    }
    
    const header = function (data) {
      doc.setFontSize(18);
      doc.setTextColor(30);
      doc.setFontStyle('normal');
//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
      doc.text("Report for Product List", data.settings.margin.left, 50);
  };
    
  doc.autoTable(col, rows,{
    theme : 'grid',
   /*  theme: 'striped' */
    styles: {
            halign: 'right',},
    headerStyles: {
                 fillColor: [0, 65, 69],halign:'center'},
                 margin: {top: 60}, beforePageContent: header,
        
    columnStyles:{
         0: {halign:'left'}
    },
   

});
    
    doc.save(fileName);

  }

}

}
