import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DBService } from 'src/app/services/dbservice.service';
import * as xlsx from 'xlsx';
//import * as jsPDF from 'jspdf';\
declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-quotationlist',
  templateUrl: './quotationlist.component.html',
  styleUrls: ['./quotationlist.component.css']
})
export class QuotationlistComponent implements OnInit {

  constructor(private _dbService: DBService, private _router: Router) { }

  QuoteDataArr = [];
  filteredTableDataArr: any;
  flagShowQuotationReport = true;

  async init() {
    let result = await this._dbService.getAllQuotes().toPromise();
    console.log("result:", result);
    this.QuoteDataArr = result["data"];
    this.filteredTableDataArr = this.QuoteDataArr;
  }

  async ngOnInit() {
    await this.init();
  }


  onClickEdit(obj) {
    this._router.navigate(['/quotation/createquotation', obj.quote_id]);
  }

  async onClickDelete(obj) {
    console.log("will delete quote:::", obj);
    await this._dbService.deleteQuote(obj).toPromise();
    await this.init();
  }



  search(term: string) {
    let fieldName = "customername";
    this.filteredTableDataArr = this.QuoteDataArr.filter(x =>
      x[fieldName].trim().toLowerCase().includes(term.trim().toLowerCase())
    );
  }
  fileName= 'Quotationlist.excel'; 
  exportToExcel() {
    let element = null;
    if (this.flagShowQuotationReport) { 
      this.fileName = "Quotationlist.xlsx";
      //let element = document.getElementById('content4'); 
      const ws: xlsx.WorkSheet = xlsx.utils.json_to_sheet(this.QuoteDataArr);
      const wb: xlsx.WorkBook = xlsx.utils.book_new();
      xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
  
      /* save to file */
      xlsx.writeFile(wb, this.fileName);
    
    }
  }
  downloadPdf(){
    let data = null;
    const doc = new jsPDF();
    let fileName = "Quotationlist.pdf";
    if (this.flagShowQuotationReport) {
      fileName = "Quotationlist.pdf";
      //data = document.getElementById('content2');
      const col = ["Quotation number", "Vendor Name", "Mobile", "Quotation Date","Product Name"];
      const rows = [];
       
    for (let k = 0; k < this.QuoteDataArr.length; k++) {
      var temp = [this.QuoteDataArr[k].quotationnumber,this.QuoteDataArr[k].vendorname, this.QuoteDataArr[k].mobile,
      this.QuoteDataArr[k].quotationdate, this.QuoteDataArr[k].productname
      
      ];
      rows.push(temp);
      }
      const header = function (data) {
        doc.setFontSize(18);
        doc.setTextColor(30);
        doc.setFontStyle('normal');
  //doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
        doc.text("Report for Quotation List", data.settings.margin.left, 50);
    };
      
    doc.autoTable(col, rows,{
      theme : 'grid',
     /*  theme: 'striped' */
      styles: {
              halign: 'right',},
      headerStyles: {
                   fillColor: [0, 65, 69],halign:'center'},
                   margin: {top: 60}, beforePageContent: header,
          
      columnStyles:{
           0: {halign:'left'}
      },
     
  
  });
      
     
      doc.save(fileName);
  
    }
  
  }

}
