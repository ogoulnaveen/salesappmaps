import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Navigation',
    main: [
      {
        state: 'dashboard',
        name: 'Dashboardmenu',
        type: 'link',
        icon: 'ti-dashboard'
      }
    ],
  },
  /*  {
    label: 'Navigation',
    main: [
      {
        state: 'visits',
        name: 'Visits',
        type: 'link',
        icon: 'ti-home'
      }
    ],
  }, */
  {
    label: 'Navigation',
    main: [
      {
        state: 'visitmode',
        name: 'Visitsmenu',
        type: 'sub',
        icon: 'ti-line-double',
        children: [
          {
            state: 'addvisit',
            name: 'Addvisitmenu'
          },
          {
            state: 'searchvisits',
            name: 'Searchvisitsmenu'
          },
         
         
        ]
      }
    ],
  },
  {
    label: 'Navigation',
    main: [
      {
        state: 'managevisit',
        name: 'Assignvisitsmenu',
        type: 'sub',
        icon: 'ti-line-double',
        children: [
          {
            state: 'schedulevisit',
            name: 'Schedulevisitsmenu'
          },
        ]
      }
    ],
  },
  {
    label: 'Navigation',
  main: [
      {
        state: 'expense',
        name: 'expensesmenu',
        type: 'sub',
        icon: 'ti-stats-up',
        children: [
          {
            state: 'addexpense',
            name: 'Addexpensemenu'
          },
          {
            state: 'expenselist',
            name: 'showallexpensemenu'
          }
        ]
      }
    ],
  },

  {
    label: 'UI Element',
    main: [
      {
        state: 'user',
        name: 'Usersmenu',
        type: 'sub',
        icon: 'ti-user',
        children: [
          {
            state: 'adduser',
            name: 'AddUsersmenu'
          },
          {
            state: 'userlist',
            name: 'Showallusersmenu'
          },
          {
            state: 'addusertype',
            name: 'AddUserstypemenu'
          },
          {
            state: 'usertypelist',
            name: 'Usertypelistmenu'
          }
        ]
      },
     
    ]
  },
  


  
  


  
  {
    label: 'UI Element',
    main: [
      {
        state: 'products',
        name: 'ProductMenu',
        type: 'sub',
        icon: 'ti-bag',
        children: [
        
          
          {
            state: 'createproduct',
            name: 'Addproductsmenu'
          },
          {
            state: 'productlist',
            name: 'Showallproductsmenu'
          },
          {
            state: 'categoryadd',
            name: 'Productcategorymenu'
          }, 
          {
            state: 'categorylist',
            name: 'Productcategorylistmenu'
          }
         
        ]
      },
     
    ]
  },
  

  
  {
    label: 'Navigation',
    main: [
      {
        state: 'quotation',
        name: 'Quotationmenu',
        type: 'sub',
        icon: 'fa fa-book',
        children: [
          {
            state: 'createquotation',
            name: 'AddQuotationmenu'
          },
          {
            state: 'quotationlist',
            name: 'Quotationlistmenu'
          }
        ]

      }
    ],
  },














  {
    label: 'Navigation',
    main: [
      {
        state: 'vendors',
        name: 'Customersmenu',
        type: 'sub',
        icon: 'fa-handshake-o',
        children: [
          {
            state: 'createvendor',
            name: 'AddCustomersmenu'
          },
          {
            state: 'vendorlist',
            name: 'Customerslistmenu'
          }
        ]

      }
    ],
  },


  /* {
    label: 'Navigation',
    main: [
      {
        state: 'quotationlist',
        name: 'quotationlist',
        type: 'link',
        icon: 'ti-home'
      }
    ],
  }, */


  

 
/*
  
  {
    label: 'Forms',
    main: [
      {
        state: 'forms',
        name: 'Form Components',
        type: 'link',
        icon: 'ti-layers'
      }
    ]
  },
  {
    label: 'Tables',
    main: [
      {
        state: 'bootstrap-table',
        name: 'Bootstrap Table',
        type: 'link',
        icon: 'ti-receipt'
      }
    ]
  },
  {
    label: 'Map',
    main: [
      {
        state: 'map',
        name: 'Maps',
        type: 'link',
        icon: 'ti-map-alt'
      }
    ]
  },
  {
    label: 'Pages',
    main: [
      {
        state: 'auth',
        short_label: 'A',
        name: 'Authentication',
        type: 'sub',
        icon: 'ti-id-badge',
        children: [
          {
            state: 'login',
            type: 'link',
            name: 'Login',
            target: true
          }, {
            state: 'registration',
            type: 'link',
            name: 'Registration',
            target: true
          }
        ]
      }
    ]
  },
  {
    label: 'Other',
    main: [
      {
        state: '',
        name: 'Menu Levels',
        type: 'sub',
        icon: 'ti-direction-alt',
        children: [
          {
            state: '',
            name: 'Menu Level 2.1',
            target: true
          }, {
            state: '',
            name: 'Menu Level 2.2',
            type: 'sub',
            children: [
              {
                state: '',
                name: 'Menu Level 2.2.1',
                target: true
              },
              {
                state: '',
                name: 'Menu Level 2.2.2',
                target: true
              }
            ]
          }, {
            state: '',
            name: 'Menu Level 2.3',
            target: true
          }, {
            state: '',
            name: 'Menu Level 2.4',
            type: 'sub',
            children: [
              {
                state: '',
                name: 'Menu Level 2.4.1',
                target: true
              },
              {
                state: '',
                name: 'Menu Level 2.4.2',
                target: true
              }
            ]
          }
        ]
      },
      
      {
        state: 'simple-page',
        name: 'Simple Page',
        type: 'link',
        icon: 'ti-layout-sidebar-left'
      }
    ]
  }*/
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
