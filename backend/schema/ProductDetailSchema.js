const mongoose = require("mongoose");

const ProductDetailSchema = mongoose.Schema({
    product_id: { type: String, required: true },
    productname: { type: String},
    cost: { type: String},
    suppliername: { type: String},
    categoryname: {type: String}
});
module.exports = mongoose.model('ProductDetails', ProductDetailSchema, 'product_details');
