import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DBService } from 'src/app/services/dbservice.service';
import * as xlsx from 'xlsx';
//import * as jsPDF from 'jspdf';\
declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-vendorlist',
  templateUrl: './vendorlist.component.html',
  styleUrls: ['./vendorlist.component.css']
})
export class VendorlistComponent implements OnInit {

 constructor(private _dbService: DBService, private _router: Router) { }
 
 VendorDataArr = [];
 filteredTableDataArr: any;
 flagShowVendorReport= true;

 async init() {
    let result = await this._dbService.getAllVendors().toPromise();
    console.log("result:", result);
    this.VendorDataArr = result["data"];
    this.filteredTableDataArr = this.VendorDataArr;

  }
  
  async ngOnInit() {
    await this.init();
  }
  
 
 onClickEdit(obj) {
    this._router.navigate(['/vendors/createvendor', obj.vendor_id]);
  }
  
   async onClickDelete(obj) {
    await this._dbService.deleteVendor(obj).toPromise();
    await this.init();
  }

  search(term: string) {
    let fieldName = "customer_name";
    this.filteredTableDataArr = this.VendorDataArr.filter(x =>
      x[fieldName].trim().toLowerCase().includes(term.trim().toLowerCase())
    );
  }
   
  fileName= 'Vendorlist.excel'; 
exportToExcel() {
  let element = null;
  if (this.flagShowVendorReport) { 
    this.fileName = "Customerlist.xlsx";
    //let element = document.getElementById('content4'); 
    const ws: xlsx.WorkSheet = xlsx.utils.json_to_sheet(this.VendorDataArr);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    xlsx.writeFile(wb, this.fileName);
  
  }
}
downloadPdf(){
  let data = null;
  const doc = new jsPDF();
  let fileName = "Customerlist.pdf";
  if (this.flagShowVendorReport) {
    fileName = "Vendorlist.pdf";
    //data = document.getElementById('content2');
    const col = ["Vendor Name","Email", "Mobile", "First Name","Last Name"];
    const rows = [];
     
  for (let k = 0; k < this.VendorDataArr.length; k++) {
    var temp = [this.VendorDataArr[k].vendor_name,this.VendorDataArr[k].email, this.VendorDataArr[k].phone,
    this.VendorDataArr[k].first_name, this.VendorDataArr[k].last_name
    
    ];
    rows.push(temp);
    }
    const header = function (data) {
      doc.setFontSize(18);
      doc.setTextColor(30);
      doc.setFontStyle('normal');
//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
      doc.text("Report for Vendor List", data.settings.margin.left, 50);
  };
    doc.autoTable(col, rows,{
      theme : 'grid',
     /*  theme: 'striped' */
      styles: {
              halign: 'right',},
      headerStyles: {
                   fillColor: [0, 65, 69],halign:'center'},
                   margin: {top: 60}, beforePageContent: header,
          
      columnStyles:{
           0: {halign:'left'}
      },
     

  });


    doc.save(fileName);

  }

}

}
