import { Component, OnInit, ViewChild } from '@angular/core';
import { DBService } from 'src/app/services/dbservice.service';



@Component({
  selector: 'app-schedulevisit',
  templateUrl: './schedulevisit.component.html',
  styleUrls: ['./schedulevisit.component.css']
})
export class SchedulevisitComponent implements OnInit {

  keyword = 'username';
  customer_keyword = "customer_name";
  salesUsers;
  vendorLists;
  selectedDate;
  
  constructor(private _dbService: DBService) { 
    
  }
  ngOnInit() {
    //naveen - get all visits from db
    this._dbService.getAllVendors().subscribe(res => {
      if (res) {
        this.vendorLists = res.data;
      }
    });
    //naveen - get all sales users to populate in autocomplete dropdown 
    this._dbService.getUsersByUserType({userType: 'Sales user'}).subscribe(res =>{
      this.salesUsers = res.data;
    })
  }
  selectEvent(customer) {
    
  }
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    // do something when input is focused
  }

  scheduleVisits(){
    
  }
}

