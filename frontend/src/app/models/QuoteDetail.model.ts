export class QuoteDetail {
    quote_id: string;
    quotationnumber: string;
    vendorname : string;
    mobile : string;
    quotationdate : string;
    productname : string;
    categoryname: string;   
    qnty : string;
    rate : string;
    discount  : string;
    total  : string;
    
   
}