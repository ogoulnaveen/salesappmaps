import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { UserDetail } from '../models/UserDetail.model';
import { ProductDetail } from '../models/ProductDetail.model';
import { QuoteDetail } from '../models/QuoteDetail.model';
import { VendorDetail } from '../models/VendorDetail.model';
import { CategoryDetail } from '../models/CategoryDetail.model';
import { UserType } from '../models/UserType.model';
import { throwError } from 'rxjs/internal/observable/throwError';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class DBService {

    SERVER_URL = "http://localhost:3000/all";
    //SERVER_URL = "http://localhost:3000/all";
    
    constructor(private _http: HttpClient) {
    }

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }
    // Handle API errors
    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };
    
    
    register(user): Observable<any> {
        return this._http
            .post<any>(this.SERVER_URL + '/register', user, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
    login(user): Observable<any> {
        return this._http
            .post<any>(this.SERVER_URL + '/login', user, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
    addUser(obj: UserDetail) {
        console.log("Inside addUser");
        return this._http.post(this.SERVER_URL + '/addUser', obj);
    }

    addProduct(obj: ProductDetail) {
        console.log("Inside addProduct");
        return this._http.post(this.SERVER_URL + '/addProduct', obj);
    }

    addQuote(obj: QuoteDetail) {
        console.log("Inside addQuote");
        return this._http.post(this.SERVER_URL + '/addQuote', obj);
    }
    addVendor(obj: VendorDetail) {
        console.log("Inside addVendor");
        return this._http.post(this.SERVER_URL + '/addVendor', obj);
    }

    updateUser(obj: UserDetail) {
        return this._http.post(this.SERVER_URL + '/updateUser', obj);
    }

    updateProduct(obj: ProductDetail) {
        return this._http.post(this.SERVER_URL + '/updateProduct', obj);
    }

    updateQuote(obj: QuoteDetail) {
        return this._http.post(this.SERVER_URL + '/updateQuote', obj);
    }
    
    updateVendor(obj: VendorDetail) {
        return this._http.post(this.SERVER_URL + '/updateVendor', obj);
    }

    getUser(user_id){
        return this._http.get(this.SERVER_URL + '/getUser/' + user_id);
    }
    getProduct(product_id){
        return this._http.get(this.SERVER_URL + '/getProduct/' + product_id);
    }
    getQuote(quote_id){
        return this._http.get(this.SERVER_URL + '/getQuote/' + quote_id);
    }
    getVendors(vendor_id){
        return this._http.get(this.SERVER_URL + '/getVendors/' + vendor_id);
    }

    
    deleteUser(obj){
        return this._http.post(this.SERVER_URL + '/deleteUser', obj);
    }
    deleteProduct(obj){
        return this._http.post(this.SERVER_URL + '/deleteProduct', obj);
    }

    deleteQuote(obj){
        return this._http.post(this.SERVER_URL + '/deleteQuote', obj);
    }
    deleteVendor(obj){
        return this._http.post(this.SERVER_URL + '/deleteVendor', obj);
    }

   
    getAllProducts(){
        return this._http.get(this.SERVER_URL + '/getAllProducts');
    }
    getAllUsers() {
        return this._http.get(this.SERVER_URL + '/getAllUsers');
    }
    
    getUsersByUserType(userType): Observable<any> {
        return this._http
            .post<any>(this.SERVER_URL + '/getUsersByUserType', userType, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
    getAllQuotes() {
        return this._http.get(this.SERVER_URL + '/getAllQuotes');
    }

    // getAllVendors() {
    //     return this._http.get(this.SERVER_URL + '/getAllVendors');
    // }
    // converted above function to observables
    getAllVendors(): Observable<any> {
        return this._http
            .get<any>(this.SERVER_URL + '/getAllVendors', this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }

    getTotalUsers(){
        return this._http.get(this.SERVER_URL + '/getTotalUsers');
    }
    getTotalQuotes(){
        return this._http.get(this.SERVER_URL + '/getTotalQuotes');
    }
    getTotalProducts(){
        return this._http.get(this.SERVER_URL + '/getTotalProducts');
    }
    getTotalVendors(){
        return this._http.get(this.SERVER_URL + '/getTotalVendors');
    }
    getTotalVisits(){
        return this._http.get(this.SERVER_URL + '/getTotalVisits');
    }
    deleteCategory(obj){
        return this._http.post(this.SERVER_URL + '/deleteCategory', obj);
    }
    getAllCategory() {
        return this._http.get(this.SERVER_URL + '/getAllCategory');
    }
    getCategory(category_id){
        return this._http.get(this.SERVER_URL + '/getCategory/' + category_id);
    }
    updateCategory(obj: CategoryDetail) {
        return this._http.post(this.SERVER_URL + '/updateCategory', obj);
    }
    addCategory(obj: CategoryDetail) {
        console.log("Inside addCategory");
        return this._http.post(this.SERVER_URL + '/addCategory', obj);
    }
    deleteUserType(obj){
        return this._http.post(this.SERVER_URL + '/deleteUserType', obj);
    }
    getAllUserType() {
        return this._http.get(this.SERVER_URL + '/getAllUserType');
    }
    getUserType(usertype_id){
        return this._http.get(this.SERVER_URL + '/getUserType/' + usertype_id);
    }
    updateUserType(obj: UserType) {
        return this._http.post(this.SERVER_URL + '/updateUserType', obj);
    }
    addUserType(obj: UserType) {
        console.log("Inside addUserType");
        return this._http.post(this.SERVER_URL + '/addUserType', obj);
    }

    //////////////////// -- visits code --////////////////////////
    getAllVisits(): Observable<any> {
        return this._http
            .get<any>(this.SERVER_URL + '/getAllVisits', this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
    getVisit(visit_id): Observable<any> {
        return this._http
            .get<any>(this.SERVER_URL + '/getVisit/'+ visit_id, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
    getVisitsBetweenDate(dates): Observable<any> {
        return this._http
            .post<any>(this.SERVER_URL + '/getVisitsBetweenDates', dates, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
    addVisit(visit): Observable<any> {
        return this._http
            .post<any>(this.SERVER_URL + '/addVisit',visit, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
    updateVisit(visit): Observable<any> {
        return this._http
            .post<any>(this.SERVER_URL + '/updateVisit',visit, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
    deleteVisit(visit): Observable<any> {
        return this._http
            .post<any>(this.SERVER_URL + '/deleteVisit',visit, this.httpOptions)
            .pipe(
                catchError(this.handleError)
            )
    }
}