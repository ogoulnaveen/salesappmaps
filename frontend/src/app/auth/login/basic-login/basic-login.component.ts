import { Component, OnInit } from '@angular/core';
import { DBService } from 'src/app/services/dbservice.service';
import { Router } from '@angular/router';
import { Utility } from 'src/app/services/utility.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit {
  email: string;
  password: string;
  lang: string ="";
  constructor( private _dbService: DBService, private _router: Router, private util: Utility,public translate: TranslateService,) { }

  ngOnInit() {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
  }

  login(){
    if(this.login && this.password){
      this._dbService.login({email:this.email,password:this.password}).subscribe(res =>{
        if(res.data){
          localStorage.setItem('jwt', res.data);
          this._router.navigate(["dashboard"]);
        }
      });
    }else{
      alert("Enter email and password");
    }
    
  }
  onLanguageSelect(ev){
    console.log(ev.target.value);
    //this.translate.use(ev.target.value);
    this.util.langDataSource.next(ev.target.value);
  }
}
