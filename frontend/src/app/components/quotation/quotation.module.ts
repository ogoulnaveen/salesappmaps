import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreatequotationComponent } from './createquotation/createquotation.component';
import { CreateqotationRoutingModule} from './quotation.routing'
import { QuotationlistComponent } from './quotationlist/quotationlist.component';
import { DataTableModule } from 'angular-6-datatable';

@NgModule({
  imports: [
  
    SharedModule,
    CreateqotationRoutingModule,
    DataTableModule
  ],
  declarations: [
    CreatequotationComponent,
    QuotationlistComponent
  ],
})
export class CreatequotationModule { }
