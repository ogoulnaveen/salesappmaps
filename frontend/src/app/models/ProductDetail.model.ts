export class ProductDetail {
    product_id: string;
    productname: string;
    cost: string;
    suppliername: string;
    categoryname: string;   
}